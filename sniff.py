from scapy.all import *
import struct
import sys
from io import StringIO
import smtplib
import threading
import json
import datetime


class IntrusionException(Exception):
    """
    Exception thrown when an intrusion is detected
    """
    def __init__(self, pkt, *args):
        super().__init__(self, *args)

        self.pkt = pkt


class MySQLLoginPacket(object):
    """
    Interprets MariaDB Login packet to extract the user name
    """
    def __init__(self, payload):
        self._payload = payload
        self.user = self._payload[32:].split(b'\x00')[0].decode('ascii')


class MySQLPacket(object):
    """
    Interprets a MariaDB packet, extracting length, sequence ID and payload
    """
    def __init__(self, payload):
        self._payload = payload
        self.length = struct.unpack('<l', self._payload[:3] + b'\0')
        self.seq_id = payload[3]
        self.payload = payload[4:]

    def login(self):
        "Transforms this packet into a login packet"
        return MySQLLoginPacket(self.payload)

    def is_quit(self):
        """
        Check if this packet is request for closing the connection with the
        server
        """
        return self.seq_id == 0 and self.payload == '\x01'


class MySQLHandler(object):
    """
    Handler for MariaDB packets, it keeps state associated with a connection
    so a packet is not mis-interpreted as a login packet.
    """
    def __init__(self):
        self._connstate = {}

    def handle(self, pkt):
        # The algorithm uses a set of 2 (IP, port) tuples to identify a
        # connection.
        id = set(
            (pkt[IP].src, pkt[TCP].sport),
            (pkt[IP].dst, pkt[TCP].dport)
        )

        if not id in self._connstate:
            self._connstate[id] = "handshake"

        if Raw not in pkt:
            return

        mypkt = MySQLPacket(pkt[Raw].load)

        if self._connstate[id] == "handshake":
            # Only process login packets during handshake
            if mypkt.seq_id == 1 and not mypkt.is_quit():
                loginpkt = mypkt.login()
                self._connstate[id] = "logged"

                if loginpkt.user != "root":
                    raise IntrusionException(
                        pkt,
                        "Invalid user for mariadb: " + loginpkt.user
                    )

        if mypkt.is_quit():
            # Remove connection on quit
            del self._connstate[id]


class SMTPMailer(object):
    """
    Class used for configuring the SMTP client and sending email.
    """
    def __init__(self, config):
        self.host = config['options']['host']
        self.port = config['options']['port']
        self.sender = config['sender']
        self.rcpt = config['rcpt']

    def send(self, message):
        "Adds extra information to the message and send an email"
        message = ("From: " + self.sender + "\r\n"
                   "To: " + self.rcpt + "\r\n"
                   "Subject: Intrusion detection\r\n") + message
        def _async():
            with smtplib.SMTP(self.host, self.port) as smtp:
                smtp.sendmail(self.sender, [self.rcpt], message)

        threading.Thread(target=_async).start()


def _build_mailer():
    with open("config.json") as f:
        conf = json.load(f)

    conf = conf["email"]

    if conf["driver"] == "smtp":
        return SMTPMailer(conf)
    else:
        raise ValueError("Invalid driver: " + conf["driver"])


handle_mysql = MySQLHandler()
mailer = _build_mailer()


def handle_packet(pkt):
    # if it is not an IP packet, ignore it
    if not IP in pkt:
        return

    my_ip = "192.168.0.1"
    orome = "192.168.0.101"
    allowed_ips = ["192.168.0.1", "192.168.0.2"]

    # If packet comes or goes to the mail/dns/dhcp server, ignore it
    if pkt[IP].src == orome or pkt[IP].dst == orome:
        return

    try:
        # if the ip from the host receiving or sending a packet is not the
        # secure machine (192.168.0.2) or this machine (192.168.0.1)
        # Informs Intrusion
        if pkt[IP].src not in allowed_ips:
            raise IntrusionException(
                pkt,
                "Packet from unsafe host: " + pkt[IP].src
            )
    
        if pkt[IP].dst not in allowed_ips:
            raise IntrusionException(
                pkt,
                "Packet to unsafe host: " + pkt[IP].dst
            )

        # If it is an TCP packet, only accepts packets coming into the
        # 8306 or 5080 ports, otherwise, report it
        if TCP in pkt:
            port = pkt[TCP].dport
            if pkt[IP].src != my_ip and port != 8306 and port != 5080:
                raise IntrusionException(
                    pkt,
                    "Packet incoming in an insecure TCP port: {}".format(port)
                )

            if pkt[IP].src != my_ip and (port == 8306 or port == 3306):
                handle_mysql.handle(pkt)

            return
        # If it is not TCP, report it
        else:
            raise IntrusionException(pkt, "Packet using invalid protocol")

    except IntrusionException as ex:
        # To get a full report capture the exit of the show() method to the
        # stdout
        sys_stdout = sys.stdout
        sys.stdout = strout = StringIO()

        ex.pkt.show()
        sys.stdout = sys_stdout

        # Send email, if any intrusion was detected
        mailer.send(
            "An intrusion was detected at varda.sas.virtual\n"
            "Message: " + ex.args[1] +
            "\nDate/Time: " + datetime.datetime.now().isoformat() +
            "\nFull Information:\n" + strout.getvalue()
        )


if __name__ == '__main__':
    iface = os.environ.get("SNIFF_IFACE", None)

    sniff(iface=iface, prn=show_packet)

